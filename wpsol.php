<?php
/*
Plugin Name: Sol-Connector
Description: Connect WordPress to the Scouting Nederland OpenID Server
Author: Martijn Papendrecht
Version: 0.0.1
##ORIGINAL AUTHOR DATA
##Plugin URI: https://github.com/koter84/wpSOL
##Author: Dennis Koot
##Author URI: http://wordpress.org/plugins/wpsol/
##Version: 1.1.11
##License: GPLv2 or later
##Text Domain: wpSOL
*/

include_once 'openid.php';
include_once 'common.php';

// Init wpsol-plugin
function wpsol_init()
{
	// Translation-support (i18n)
	load_plugin_textdomain('wpsol', false, 'wpsol/languages');

	// Gebruikersnaam veld toevoegen aan de login pagina
	// add_action('login_form', 'wpsol_wp_login_form');
	add_filter('login_form_middle', 'wpsol_wp_login_form_middle');

	// Inhaken op het authenticatie process
	add_filter('authenticate', 'wpsol_authenticate_username_password', 9);

	// Geef extra links in de plugin-overzichtspagina
	add_filter('plugin_action_links_'.plugin_basename(__FILE__), 'wpsol_plugin_action_links');
}
add_action('plugins_loaded', 'wpsol_init');

function wpdocs_register_widgets() {
	register_widget( 'Sol_Widget');
}
add_action( 'widgets_init', 'wpdocs_register_widgets' );

// Remove admin bar voor niet admins
add_action('after_setup_theme', 'remove_admin_bar');
 
function remove_admin_bar() {
if (!current_user_can('administrator') && !is_admin()) {
  show_admin_bar(false);
}
}

// Init wpsol-admin
function wpsol_admin_menu()
{
	add_options_page('wpSOL', 'Sol-Connector', 'manage_options', 'wpsol_settings', 'wpsol_admin_options');

}
add_action('admin_menu', 'wpsol_admin_menu');

function test_logout($redirect_to, $requested_redirect_to, $user){
	wpsol_logout_redirect();
}

add_filter('logout_redirect', 'test_logout', 10, 3);

// Setup defaults during installation
register_activation_hook( __FILE__, 'wpsol_install');
